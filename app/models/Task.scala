package models

import anorm._
import anorm.SqlParser._
import play.api.db._
import play.api.Play.current

case class Task(id: Long, label: String)

object Task {

	//Parseador de tareas al obtenerlas de la BD
	val task = {
		get[Long]("id") ~ 
		get[String]("label") map {
			case id~label => Task(id,label)
		}
	}

	//Obtiene todas las tareas de la BD
	def all(): List[Task] = DB.withConnection { implicit c => 
		SQL("select * from task").as(task *)
	}

	//Inserta una tarea en la BD asignandole el parametro label como dicho valor.
	def create(label: String) {
		DB.withConnection { implicit c =>
			SQL("insert into task (label) values ({label})").on(
				'label -> label
			).executeUpdate()
		}
	}

	//Borra de la BD la tarea con el id pasado por parametro.
	def delete(id: Long) {
		DB.withConnection{ implicit c =>
			SQL("delete from task where id = {id}").on(
				'id -> id
			).executeUpdate()
		} 
	}
	
}