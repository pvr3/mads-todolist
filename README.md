# Corrección de la práctica

Calificación: **0,4**

Muy bien, pero el resumen te ha salido algo extenso. La próxima vez resúmelo un poco más. 

También tienes algún error importante de concepto como la frase:

>Aqui `request` es la peticion HTTP que ha ejecutado la Action

Repásalo porque las peticiones y los *request* no "ejecutan" acciones. 

Nota sobre Markdown: no utilices las citas (">") por defecto para un apartado. Sólo se usa para citar un texto.

30 de septiembre  
Domingo Gallardo

-----------


#Práctica 1 - Introduccion a Play Framework
##Alumno: Pablo Verdú Romero  
------------------------------------------------------------
###1. Introduccion

>El proposito de la práctica es la familiarización con el framework Play para el lenguaje Scala. Para ello se debía implementar el tutorial [*You're first Application*](http://www.playframework.com/documentation/2.2.x/ScalaTodoList) de la página del framework. Además, tambien se solicitaba el aprendizaje de las herramientas Git usando como repositorio remoto Bitbucket y la herramienta de despliegue Heroku para publicar la aplicacion. La aplicación la tengo desplegada en [**esta**](http://mads-todolist.herokuapp.com) dirección.

>También se pide el realizar esta memoria con los concpetos adquiridos durante el desarrollo de la práctica. Concretamente un resumen de las paginas:

>-  [You're first application](#markdown-header-2-youre-first-application)
>-  [Actions, Controllers and Results](#markdown-header-3-actions)
>-  [HTTP Routing](#markdown-header-4-routes)
>-  [Manipulating Results](#markdown-header-5-manipulando-resultados)

###2. You're first application

>Esta página propone la implementación de tu primera aplicación utilizando el framework Play y subirla a la nube. La aplicación en concreto es una lista de tareas.

>En la introduccion nos recomienda que utilicemos un IDE para el desarrollo del tutorial, no obstante, dice que tambien se puede utilizar un editor de texto, puesto que el proceso de compilacion lo realizan las propias herramientas del framework.

>####2.1 Creación de un proyecto

>Para crear una nueva aplicacion en Play tenemos que utilizar este comando:
>>`$ play new <nombre app>`

>Ese comando se encarga de crear un directorio con el nombre de la aplicacion que contine todo el arbol de directorios de un proyecto Play. Estos directorios son los siguientes:

>>- `app/`: contiene el codigo de la aplicacion en si, dividido en los subdirectorios models, views y controllers (Siguiendo la arquitectura modelo-vista-controlador).

>>- `conf/`: contiene los ficheros de configuración del proyecto, entre ellos el main (*application.conf*), el fichero *routes* con las direcciones de la aplicación y *messages* para traducciones.

>>- `project/`: con los scripts de construcción de la aplicación.

>>- `public/`: contiene los recursos publicos de la aplicacion como ficheros Javascript, hojas de estilo, imagenes...

>>- `test/`: contiene los tests de la aplicacion en formato Specs2.

>####2.2 Consola

>>En este apartado del tutorial se nos enseña el uso de la consola de aplicacion de Play, ademas de como lanzar nuestra aplicacion de manera local utilizando esta direccion *http://localhost:9000*.

>####2.3 Vision general

>>Una vez te han explicado como desplegar tu aplicacion vacia en este apartado te cuentan como se visualiza la pagina de tu aplicacion.

>>Este apartado te informa de que el archivo principal de tu aplicacion es el fichero `conf/routes`, en el que se encuentran todas las URLs accesibles en tu aplicación. En un proyecto vacio se genera por defecto la siguiente peticion:


>>      GET         /               controllers.Application.index
                
>>Lo cual significa que cuando el servidor web reciba una peticion HTTP de tipo GET solicitando la pagina raiz se maneje mediante la *Action* `controllers.Application.index`.

>>Nota: Ver mas informacion sobre Routes, [aquí](#markdown-header-4-routes).

>>Si abrimos el fichero `app/controllers/Application.scala` encontramos este codigo:

>>      package controllers 
>>      import play.api.mvc._

>>      object Application extends Controller {
>>          def index = Action {
>>              Ok(views.html.index("Your new application is ready."))
>>          }
>>      }
                
>>Esta singleton es la clase que contiene los controladores principales de la aplicacion, entre ellos, las *Action* que manejaran las peticiones HTTP declaradas en el fichero `conf/routes`. En este caso concreto la *Action* index es una funcion anonima de scala la cual cuando se ejecuta invoca a la funcion Ok que devuelve al servidor web una respuesta HTTP **200 OK** con contenido HTML. Dicho contenido es un template del framework que es compilado como una funcion Scala.

>>Nota: Ver más información sobre Actions [aquí](#markdown-header-3-actions)

>>Dicho template se encuentra en la ruta `app/views/index.scala.html` cuyo contenido es:

>>      @(message: String)
>>      
>>      @main("Welcome to Play") {
>>          @play20.welcome(message)
>>      }
                
>>Para introducir codigo Scala en los templates de Play Framework se utiliza la notacion *@* antes de dicho codigo. Si la pagina HTML debe recibir parametros se especifican en la primera linea (En este caso la pagina recibe un String llamado message).


>####2.4 Preparando la aplicación.

>>Despues de introducirnos un poco en el funcionamiento basico de Play, el tutorial prosigue con el desarrollo de la aplicación en sí.

>>Para ello, lo primero que solicita el tutorial es que en el fichero `conf/routes` añadamos la posibilidad de que se pueda realizar una llamada GET sobre la ruta */tasks* y dos llamadas POST una sobre */tasks* tambien y la otra sobre */task/:id/delete/*. En la ultima como se puede observar se introduce un parametro en la URL. Las *Action* que manejaran dichas peticiones seran, respectivamente, `controllers.Application.tasks`, `controllers.Application.newTask` y `controllers.Application.deleteTask(id: Long)`. Se puede observar que en la *Action* que maneja la URL con parametro se le añade dicho parametro a la funcion anonima de Scala.

>>Dichas *Actions* hay que definirlas correctamente para que la aplicacion funcione, para ello y hasta que las implementemos el tutorial nos indica que añadamos lo siguiente en el fichero `Application.scala`:

>>      object Application extends Controller {
  
>>	     def index = Action {
>>	         Redirect(routes.Application.tasks)
>>	     }
  
>>	     def tasks = TODO
  
>>	     def newTask = TODO
  
>>	     def deleteTask(id: Long) = TODO
  
>>	    }

>>La funcion TODO devuelve una respuesta HTTP `501 Not Implemented` que indica que dicha URL no esta implementada todavia. Además, se modifica la *Action* `index` añadiendo una llamada a la funcion `Redirect` pasandole como parametro otra *Action* a la cual quieres que se redirija.

>####2.5 Preparando el Modelo.

>>El siguiente paso es crear el modelo de la aplicacion de prueba. Creamos un directorio `app/models` y en el crearemos la clase `Task.scala`:

>>      package models

>>      case class Task(id: Long, label: String)

>>      object Task {
  
>>        def all(): List[Task] = Nil
  
>>        def create(label: String) {}
  
>>        def delete(id: Long) {}
  
>>      }

>>Como se puede observar se trata de una case class con un objeto compañero con tres metodos estaticos de clase que se encaragaran del "CRUD". De momento, no se implementan.

>####2.6 Template de la aplicación.

>>Como vista de nuestra aplicacion usaremos este template que muestra la lista de tareas y el form de creacion de una tarea.

>>      @(tasks: List[Task], taskForm: Form[String])

>>      @import helper._

>>      @main("Todo list") {
    
>>        <h1>@tasks.size task(s)</h1>
    
>>        <ul>
>>          @tasks.map { task =>
>>              <li>
>>                  @task.label
                
>>                  @form(routes.Application.deleteTask(task.id)) {
>>                      <input type="submit" value="Delete">
>>                  }
>>              </li>
>>          }
>>        </ul>
    
>>        <h2>Add a new task</h2>
    
>>        @form(routes.Application.newTask) {
        
>>            @inputText(taskForm("label")) 
        
>>            <input type="submit" value="Create">
        
>>        }
    
>>      }

>>Dicho template recibe como parametros una lista de tareas y un form (Que crearemos en el controlador) para insertar una nueva tarea. Se mapea la lista de tareas y para cada una de ellas se muestra su etiqueta y un boton que llamaria a la *Action* que borra una tarea. Por ultimo, hay un form con un input y un boton para crear una tarea, dicho boton lanzaria la *Action* `newTask` que crea una tarea. Podemos observar que el inputText de este form lo hemos creado en codigo Scala, mas adelante veremos porque.

>####2.7 El *Form Tarea*

>>Un `Form` en Play encapsula un form HTML con validacion de datos. Para nuestra aplicacion definiremos en el apartado de los controladores este form para añadir una tarea:

>>      import play.api.data._
>>      import play.api.data.Forms._

>>      val taskForm = Form(
>>        "label" -> nonEmptyText
>>      ) 

>>Este form tiene la propiedad "label", la cual, no debe tener texto vacio. Si el usuario intentara introducir el texto vacio la funcion `nonEmptyText` mostraria un mensaje de error al lado del form.

>####2.8 Mostrando la pagina principal

>>Una vez tenemos todo lo anterior (El template y el form), es el momento de modificar la *Action* `tasks` que habiamos definido antes en el fichero `Application.scala` para que muestre el template que hemos implementado. Para ello añadimos en su cuerpo la siguiente linea, sustituyendola por el `TODO`:

>>      Ok(views.html.index(Task.all(), taskForm))

>>Esto lo que hara es que cuando accedamos a la ruta `/tasks` envie una respuesta HTTP **200 OK** y nos muestre el template que hemos creado, pasandole como parametros una lista de todas las tareas (Con el metodo `Task.all()`) y el form que hemos creado en el apartado anterior.

>>Cuando ejecutamos en local la aplicacion el resultado es el siguiente:

>>![paginaInicial](http://www.playframework.com/documentation/2.2.x/resources/manual/scalaGuide/tutorials/todolist/images/blank.png)

>####2.9 Manejando la logica del form.

>>Si ahora pulsaramos el boton de crear una tarea, nos llevaria a una la pagina predefinida por el `TODO` para evitarlo vamos a modificar la *Action* `newTask` con el siguiente codigo:

>>      def newTask = Action { implicit request =>
>>        taskForm.bindFromRequest.fold(
>>          errors => BadRequest(views.html.index(Task.all(), errors)),
>>          label => {
>>            Task.create(label)
>>            Redirect(routes.Application.tasks)
>>          }
>>        )
>>      }

>>Ahora la Action que se ejecutaria al pulsar el boton recibiria los datos de la peticion con el `implicit request` los cuales se extraen del form con el metodo `bindFromRequest` y se mira con un switch de Scala si llevan errores, mandando respuesta HTTP de error y redirigiendo a la vista de tareas con los errores y si es correcta llamando al metodo `Task.create(label)`y redirigiendo de manera normal a la lista de tareas.

>####2.10 Persistiendo los datos.

>>Ahora ya podemos añadir tareas a nuestra lista, aunque para que sean persistentes necesitamos almacenarlas en una Base de Datos. Para ello definiremos un Script SQL que cree una tabla donde almacenar tareas en la ruta `conf/evolutions/default/1.sql`.

>>Una vez tenemos el esquema de la base de datos es el momento de actualizar nuestra aplicacion para que interactue con ella, dando persistencia a nuestras tareas. Play utiliza **Anorm** para definir un parseador de JDBC `ResultSet` a una `Task` de nuestra aplicacion. El parseador lo definiremos asi:

>>      import anorm._
>>      import anorm.SqlParser._

>>      val task = {
>>        get[Long]("id") ~ 
>>        get[String]("label") map {
>>          case id~label => Task(id, label)
>>        }
>>      }

>>Sin interpretamos el codigo Scala podemos observar que la propiedad task de nuestra clase sera el parseador el cual obtiene un id de tipo `Long` y una label de tipo `String` y por cada pareja de id y label crea una Tarea pasandole dichos valores como parametros. Este parseador nos vendra muy bien para listar todas las tareas en nuestra vista.

>>Ahora podemos pasar a implementar los metodos que crean y borran una tarea de la BD y el que lista todas las tareas, empezamos por este ultimo:

>>      import play.api.db._
>>      import play.api.Play.current

>>      def all(): List[Task] = DB.withConnection { implicit c =>
>>        SQL("select * from task").as(task *)
>>      }

>>El metodo `all()` devolvera una lista de tareas la cual se genera obteniendo una conexion con la BD y en dicha conexion haciendo una Select de las tareas parseandolas con el parseador que hemos implementado antes.

>>Los metodos para crear y borrar tareas son mas parecidos entre ellos:

>>      def create(label: String) {
>>        DB.withConnection { implicit c =>
>>          SQL("insert into task (label) values ({label})").on(
>>            'label -> label
>>          ).executeUpdate()
>>        }
>>      }

>>

>>      def delete(id: Long) {
>>        DB.withConnection { implicit c =>
>>          SQL("delete from task where id = {id}").on(
>>            'id -> id
>>          ).executeUpdate()
>>        }
>>      }

>>En esta ocasion lo que vemos es como tomar como parametro de una consulta SQL un parametro de una funcion Scala y que para las operaciones de inserccion debemos ejecutar el metodo `executeUpdate` de la clase SQL. Ahora ya tenemos la aplicacion operativa. Tan solo debemos implementar la ultima *Action* `deleteTask` de la siguiente forma:

>>      def deleteTask(id: Long) = Action {
>>        Task.delete(id)
>>        Redirect(routes.Application.tasks)
>>      }

>>Lo que hace esta action es llamar al metodo correspondiente de la clase tareas y despues redirigirte a la vista principal.

>>Por ultimo, en la pagina explican como desplegar tu aplicacion en **Heroku**, que es de manera similar a la que explica el enunciado de la practica unicamente hay que añadir como dependencia del proyecto los drivers de *PostgreSQL* con la que trabaja Heroku.
 
                    
###3. Actions

>####3.1 ¿Que es una Action?

>>La mayoria de peticiones que recibe Play Framework se manejan mediante `Actions`. Basicamente es una funcion que recibe una peticion y obtiene un resultado de ella.

>>El resultado es una respuesta HTTP.

>####3.2 Construyendo una Action.

>>Existen varias maneras de construir una `Action` la mas simple es la siguiente:

>>      Action {
>>        Ok("Hello world")
>>      }

>>De esta forma no tenemos referencia de que recurso ha "ejecutado" esta action y simplemente lanza el resultado. Si quisieramos acceder a la peticion HTTP que ejecuta tendriamos que construirla asi:

>>      Action { request =>
>>        Ok("Got request [" + request + "]")
>>      }

>>Aqui `request` es la peticion HTTP que ha ejecutado la `Action`. Si queremos trabajar con algun parametro implicito con alguna API tendriamos que construirla asi:

>>      Action { implicit request =>
>>        Ok("Got request [" + request + "]")
>>      }

>>Por ultimo, podriamos necesitar que el argumento de la peticion se parseara de alguna forma para ello construiriamos la `Action` asi:

>>      Action(parse.json) { implicit request =>
>>        Ok("Got request [" + request + "]")
>>      }

>####3.3 Controllers como generadores de Actions.

>>Las `Action` se generan dentro de clases que hereden de la singleton `Controller`. La manera usual de implementarlas dentro de dicha clase es como metodos que no reciben parametros:

>>      package controllers

>>      import play.api.mvc._

>>      object Application extends Controller {

>>        def index = Action {
>>          Ok("It works!")
>>        }
    
>>      }

>>Tambien se podrian construir con parametros, como hemos visto en el desarrollo de la practica.

>####3.4 Simple Result

>>El resultado de una `Action` viene definido por un objeto de la clase `SimpleResult` que encapsula una respuesta HTTP con su codigo, su cabecera y su contenido. Por tanto podemos definir un resultado de una `Action` de esta manera:

>>      def index = Action {
>>        SimpleResult(
>>          header = ResponseHeader(200, Map(CONTENT_TYPE -> "text/plain")), 
>>          body = Enumerator("Hello world!")
>>        )
>>      }

>>Aqui estamos definiendo una respuesta HTTP **OK 200** que contiene texto plano y cuyo contenido es la cadena *Hello world!*

>>Play framework tiene de numerosas opciones para construir estas respuestas de manera sencilla ya sean de Error, Redireccion...

>>      val ok = Ok("Hello world!")
>>      val notFound = NotFound
>>      val pageNotFound = NotFound(<h1>Page not found</h1>)
>>      val badRequest = BadRequest(views.html.form(formWithErrors))
>>      val oops = InternalServerError("Oops")
>>      val anyStatus = Status(488)("Strange response type")

>>[Este enlace](http://www.playframework.com/documentation/2.2.x/api/scala/index.html#play.api.mvc.Results) lleva a al trait `Result` el cual contiene el amplio catalogo de respuestas predefinidas del que dispone Play. 

>>Por ultimo, cabe destacar que las respuestas que mas nos pueden ayudar son `Redirect` la cual redirige a una nueva URL pudiendo añadir ademas un status a dicha respuesta y `TODO` que te redirige a una pagina predefina de Play que informa que dicha `Action` todavia no esta implementada.

###4. Routes

>Esta pagina, explica las nociones basicas del rutado HTTP por parte de Play Framework.

>####4.1 El router HTTP

>>El router es el encargado de transformar las peticiones HTTP en `Actions` del framework.

>>Estas peticiones contienen:

>>-  La ruta de la peticion.
>>-  El metodo HTTP (GET,POST,...)

>>Las rutas estan definidas en el fichero `conf/routes`. Este fichero es compilado y por tanto si tiene errores se visualizaran en el navegador cuando se compila la aplicacion.


>####4.2 La sintaxis del fichero Routes.

>>La manera de escribir este fichero, es como una lista en la que cada fila contiene un metodo HTTP, la ruta sobre la que se realiza la peticion y la `Action` que queremos asociar a esa peticion:

>>      GET   /clients/:id          controllers.Clients.show(id: Long)

>####4.3 Los metodos HTTP

>>Los metodos que puedes añadir a este fichero son los soportado por el protocolo HTTP  (`GET`, `POST`, `PUT`, `DELETE` y `HEAD`) 

>####4.4 La direccion URL

>>La direccion URL define la ruta de la peticion estas pueden ser estaticas y dinamicas. Las estaticas se escriben normalmente, las dinamicas tienen las siguientes opciones:

>>####Partes dinamicas

>>>     GET   /clients/:id          controllers.Clients.show(id: Long)

>>>Aqui solicitamos un cliente con su id correspondiente el parametro dentro de la direccion se especifica con `:id`. Estas direcciones solo pueden tener una unica parte dinamica.

>>####Partes dinamicas que abarcan una ruta

>>>     GET   /files/*name          controllers.Application.download(name)  

>>>Aqui solicitamos un fichero que se encuentra en una ruta la cual sera distinta cada vez, para ello usamos la notacion `*name`.

>>####Partes dinamicas con expresiones regulares personalizadas.

>>>     GET   /clients/$id<[0-9]+>  controllers.Clients.show(id: Long)

>>>Aqui solicitamos un cliente el cual solo puede ser una serie de numeros del 0 al 9 se utiliza la notacion `$<regex>` siendo regex la expresion regular que necesites.


>####4.5 Llamada al metodo Action.

>>La ultima parte de una ruta en el fichero routes es la `Action` asociada a la peticion. Lo que cabe explicar de este apartado es como encauzar los parametros de una URL como parametros de su `Action` asociada.

>>Estos parametros pueden ser extraidos de la ruta:

>>      # Extract the page parameter from the path.
>>      GET   /:page                controllers.Application.show(page)

>>O ser extraidos de la cadena de la peticion:

>>      # Extract the page parameter from the query string.
>>      GET   /                     controllers.Application.show(page)

>>Un controlador que manejaria esta ultima peticion seria el siguiente:

>>      def show(page: String) = Action {
>>          loadContentFromDatabase(page).map { htmlContent =>
>>              Ok(htmlContent).as("text/html")
>>          }.getOrElse(NotFound)
>>      }

>>En el que el parametro `page` recibido como String se carga desde la base de datos y se mapea y si cotiene contenido HTML se visualiza y si no, se envia un resultado `NotFound`.

>>Los parametros de tipo String como vemos, no hace falta tiparlos en el metodo Scala, si queremos que se transformen a otro tipo (Long, Int...) se usaria la notacion de Scala en la definicion de la `Action`: `(nombre_parametro: Tipo)`.

>>Otros aspectos interesantes es la posibilidad de fijar parametros fijos, por defecto y opcionales. Se definen de la siguiente manera, respectivamente:

>>      # Extract the page parameter from the path, or fix the value for /
>>      GET   /                     controllers.Application.show(page = "home")
>>      GET   /:page                controllers.Application.show(page)

>>

>>      # Pagination links, like /clients?page=3
>>      GET   /clients              controllers.Clients.list(page: Int ?= 1)

>>

>>      # The version parameter is optional. E.g. /api/list-all?version=3.0
>>      GET   /api/list-all         controllers.Api.list(Option[version])

>>En el primer ejemplo siempre tomara "home" como parametro, en el segundo tomara 1 en el caso de ausencia de parametro y en el tercero puedes especificar una version o no.


>####4.6 Prioridad de rutado.

>>En caso de conflicto con las rutas (Que soliciten el mismo recurso) se utilizara la que se haya declarado primero en el fichero.

>####4.7 Rutado inverso.

>>Se puede redirigir a una URL desde el codigo Scala, por ejemplo si tienes una ruta definida con su action asociada, podrias redirigir hacia a ella en el codigo Scala de esta forma:

>>      // Redirect to /hello/Bob
>>      def helloBob = Action {
>>          Redirect(routes.Application.hello("Bob"))    
>>      }

###5. Manipulando Resultados

>####5.1 Cambiando el tipo de contenido por defecto.

>>El tipo de contenido por defecto de un `Result` es inferido por Scala. Si tu añades una cadena como contenido de un `Result` sera inferido como `text/plain` o si añades codigo XML como `text/xml`, lo cual es muy util pero algunas veces desearemos modificarlo. Para ello deberemos hacerlo de la siguiente forma:

>>      val htmlResult = Ok(<h1>Hello World!</h1>).as("text/html")

>>O tambien de esta forma:

>>      val htmlResult = Ok(<h1>Hello World!</h1>).as(HTML)

>>La diferencia entre ambas, es que la segunda ademas añade a la cabecera la propiedad `charset=utf-8`, siendo por tanto mas recomendable esa.

>####5.2 Manipulando las cabeceras HTTP

>>Tambien podemos añadir o actualizar cabeceras HTTP de la siguiente forma:

>>      Ok("Hello World!").withHeaders(
>>        CACHE_CONTROL -> "max-age=3600", 
>>        ETAG -> "xx"
>>      )

>####5.3 Añadiendo y descartando Cookies.

>>Para añadir una Cookie de navegacion, tenemos que realizarlo de la siguiente forma:

>>      Ok("Hello world").withCookies(
>>        Cookie("theme", "blue")
>>      )

>>Añades a la respuesta HTTP la cookie Theme con valor blue. Para borrar una cookie debemos hacerlo de esta manera:

>>      Ok("Hello world").discardingCookies("theme")

>>Estamos descartando la cookie Theme.

>####5.4 Cambiando el juego de caracteres.

>>Play toma como juego de caracteres por defecto el `utf-8` que normalmente se utiliza, no obstante, si necesitaramos mofidificarlo podemos hacerlo.







 


  

 





